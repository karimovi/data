import json
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
file = '289b5380-c71c-42ed-b666-4b729276bd33_Data.csv'
file2 = '1fb4ead4-f83e-41ec-96e0-49a8b2fb8680_Data.csv'
file3 = 'drought.xlsx'
pd.options.display.max_columns = 999
df = pd.read_csv(file,header =0)
df2 = pd.read_csv(file2,header =0)
df3 = pd.read_excel(file3,header=0)
print(df)
df = df.rename(columns = {'2018 [YR2018]':'Agr'})
one = df[['Country Name','Country Code','Agr']]
print(df)
df2 = df2.rename(columns = {'2017 [YR2017]':'RD'})
two = df2[['Country Name','Country Code','RD']]
result = pd.merge(one,two,on='Country Name')
result2 = pd.merge(result,df3,on='Country Name')
#Export = df.to_json (r'C:\Users\Ron\Desktop\Export_DataFrame.json')
writer = pd.ExcelWriter("./data_set.xlsx", engine='xlsxwriter')
result2.to_excel(writer, sheet_name='Sheet 1')
writer.save()
writer.close()
