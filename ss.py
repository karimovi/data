import json
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler

file = './datas.xlsx'
scaler = MinMaxScaler(copy=True,feature_range=(1, 100))
df = pd.read_excel(file,header=0)
df= df.rename(columns={'calculated': 'old'})
df= df.rename(columns={'Unnamed: 10': 'calculated'})

df[['calculated']]= scaler.fit_transform(df[['calculated']])
E =  df.to_json(orient = 'records')
print(E)
print(df)