import json
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
file = "./data_predict.xlsx"
file2 = "c28666b5-a460-4590-83a9-024893af0c94_Data.csv"
file3 = "earthquake.xlsx"
df = pd.read_excel(file,header =0)
df2 = pd.read_csv(file2,header =0)
df3 = pd.read_excel(file3,header=0)
#file = open("./infrastructures.json","r")
#infs = json.load(file)
#print(infs['countries'])
data = infs['data']
#print(data[0]['indicators'][0]['values']['2017-2018'])
conts = pd.DataFrame(columns=['id','Value'])
abrev = pd.DataFrame(columns=['id','Name'])
for i in infs['countries']:
    abrev = abrev.append({'id':i['id'],'Name':i['name']},ignore_index=True)
count = 0
for i in data:
    #vac = vac.append({'Name': string[0], 'Vacancy': string[2], 'Deadline': string[1], }, ignore_index=True)
    if(i['indicators'][0]['values'].get('2017-2018') is not None):
        conts = conts.append({'id':i['id'],'Value':i['indicators'][0]['values']['2017-2018']}, ignore_index=True)
    else:
            count = count + 1


#frames = [conts,abrev]
#result = pd.concat(frames, axis=1, sort=False)
result = pd.merge(conts,abrev,on='id')
pd.options.display.max_columns = 999
#df[['Country Name','2018']]
result = result[:136]

urban = df[['Country Name','2018']]
urban= urban.rename(columns={'Country Name': 'Name','2018':'urban'})
result2 = pd.merge(result,urban,on='Name')

df2 = df2[['Country Name','average']]
df2 = df2.rename(columns={'Country Name': 'Name','average':'literacy'})
result3 = pd.merge(result2,df2,on='Name')
result4 = pd.merge(result3,df3,on='Name')
print(result4)
print(df3)
print(result2)
s = result4[['id','Name','Value','urban','literacy','numEarth','averageMov','risk']]
print(s)
#Value - Infrastucture

to_scale = s[['Value','urban','literacy']]
scaler = MinMaxScaler(copy=True,feature_range=(1, 100))
# f = scaler.fit(to_scale)
# MinMaxScaler(copy=True, feature_range=(0, 1))
# print(scaler.transform(to_scale))
#s = s.fillna(0)
s['literacy'] =s['literacy'].astype(float)
print(s[['Value','urban','literacy']])

s[['Value','urban','literacy']]= scaler.fit_transform(s[['Value','urban','literacy']])
#s[['urban']]= scaler.fit_transform(s[['urban']])
#s[['numEarth']]= scaler.fit_transform(s[['numEarth']])
#,,'literacy','risk'
print(s)
s =s.assign(calculated = 0)
s['calculated'] = (0.1*s['numEarth'])/((0.6*s['Value']) *(0.2* s['urban'])*(0.1*s['literacy']))
print(s['calculated'])
s['calculated'] = s['calculated']*1000
#s['calculated'] = scaler.fit_transform(s[['calculated']])
print(s)
E =  s.to_json(orient = 'records')
print(E)
Export = df.to_json (r'C:\Users\Ron\Desktop\Export_DataFrame.json')
writer = pd.ExcelWriter("./data.xlsx", engine='xlsxwriter')
s.to_excel(writer, sheet_name='Sheet 1')
writer.save()