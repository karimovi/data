import pandas as pd
import json
import statsmodels.api as sm
import matplotlib.pyplot as plt  # To visualize
import numpy as np
import matplotlib
file = open("./infrastructures.json",'r')
file2 = "urbanization.csv"
file3 = "earthquake.xlsx"
infs = json.load(file)
data = infs['data']
df = pd.read_csv(file2,header=0)
df2 = pd.read_excel(file3,header=0)
print(df)
#abrev = pd.DataFrame(columns=['id','Name'])
conts = pd.DataFrame(columns=['id','Value','Value2'])
#df = pd.DataFrame.from_dict(infs['data'], orient='columns')

count = 0
for i in data:
    #vac = vac.append({'Name': string[0], 'Vacancy': string[2], 'Deadline': string[1], }, ignore_index=True)
    try:
        if i['indicators'][0]['values'].get('2017-2018') is not None:
            conts = conts.append({'id':i['id'],'Value':i['indicators'][0]['values']['2017-2018'],'Value2': i['indicators'][0]['values']['2016-2017'],'Value3': i['indicators'][0]['values']['2015-2016'],'Value4': i['indicators'][0]['values']['2015-2016'],'Value5': i['indicators'][0]['values']['2014-2015'],'Value6': i['indicators'][0]['values']['2013-2014'],'Value7': i['indicators'][0]['values']['2012-2013'],'Value8': i['indicators'][0]['values']['2011-2012'],'Value9': i['indicators'][0]['values']['2010-2011'],'Value10': i['indicators'][0]['values']['2009-2010'],'Value11': i['indicators'][0]['values']['2008-2009']}, ignore_index=True)
    except KeyError:
        print("***************")
        print(i['indicators'])
    # if (i['indicators'][0]['values'].get('2016-2017') is not None):
    #     conts = conts.append({'Value2': i['indicators'][0]['values']['2016-2017']}, ignore_index=False)
    else:
            count = count + 1
df = df.rename(columns={'Country Code': 'Name'})
conts = conts.rename(columns={'id':'Name'})
print(df)
result = pd.merge(df,conts,on='Name')
df2 = df2.rename(columns={'Name':'Country Name'})
result2 = pd.merge(result,df2,on='Country Name')

pd.options.display.max_columns = 999

print(result2)

# import pandas as pd
# import statsmodels.api as sm
# df = pd.read_excel('pasha_interview_data.xls',header = 1)
# df = df.drop(df.index[6])
# print(df)
temp =  pd.DataFrame(columns=['C2019','C2020','C2021','ValueC1','ValueC2','ValueC3'])
# temp =temp.assign(C2019 = None)
# temp =temp.assign(C2020 = 0)
# temp =temp.assign(C2021 = 0)
#
# temp =temp.assign(ValueC1 = 0)
# temp =temp.assign(ValueC2 = 0)
# temp =temp.assign(ValueC3 = 0)

for i  in range(0,36):
    name = result2[['Country Name']].iloc[i].values[0]
    X = result2[['2010','2011','2012','2013','2014','2015','2016','2017','2018']]
    a = X.iloc[i].values
    y = result2[['Value','Value2','Value3','Value4','Value5','Value6','Value7','Value8','Value9']]
    b = y.iloc[i].values
    #    abrev = abrev.append({'id':i['id'],'Name':i['name']},ignore_index=True)
    #a = sm.add_constant(a)
    # print(X['const'])
    model = sm.OLS(b, a).fit()
    #IF 10 percent increase
    predictions = model.predict(a)
    # plt.scatter(a, b)
    # plt.plot(a, predictions, color='red')
    # plt.show()
    print("__________________")
    print(predictions)
    print(a)
    a= np.append(a,a[-2:-1]*1.1)
    a= np.append(a,a[-2:-1]*1.1)
    a= np.append(a,a[-2:-1]*1.1)
    print("__________________")
    print(a)
    predictions = model.predict(a)
    temp = temp.append({'Country Name':name,'C2019':a[-3],'C2020':a[-2],'C2021':a[-1],'ValueC1': predictions[-3], 'ValueC2':predictions[-2], 'ValueC3': predictions[-1]},ignore_index=True)
    #temp = temp.append({}, ignore_index=True)
    print(predictions)
print(temp)
writer = pd.ExcelWriter("./data_predict.xlsx", engine='xlsxwriter')
temp.to_excel(writer, sheet_name='Sheet 1')
writer.save()
#print(model.summary())

# print(model.predict([1,23,480,3]),'AZN')
